package com.vhome.idea;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndContentImpl;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndEntryImpl;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.feed.synd.SyndFeedImpl;
import com.rometools.rome.feed.synd.SyndImage;
import com.rometools.rome.feed.synd.SyndImageImpl;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedOutput;
import com.vhome.idea.entity.WechatDocument;

public class SogouWechatRSSGenerator {

	private static String FEED_TYPE = "rss_2.0";
	private static String FEED_TITLE = "微信公众号文章";
	private static String FEED_DESCRIPTION = "微信公众号文章 - vhomeidea";
	private static String FEED_LINK = "http://";
	private static String FEED_GENERATOR = "vhomeidea";

	private static String FEED_IMAGE_LINK = "";
	private static String FEED_IMAGE_URL = "";
	private static String FEED_IMAGE_TITLE = "";
	
	private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	public static String generateRSS(List<WechatDocument> documentList) {
		String rss = null;
		
		SyndFeed feed = new SyndFeedImpl();
		feed.setFeedType(FEED_TYPE);
		feed.setTitle(FEED_TITLE);
		feed.setDescription(FEED_DESCRIPTION);
		feed.setLink(FEED_LINK);
		feed.setGenerator(FEED_GENERATOR);

		SyndImage image = new SyndImageImpl();
		image.setLink(FEED_IMAGE_LINK);
		image.setUrl(FEED_IMAGE_URL);
		image.setTitle(FEED_IMAGE_TITLE);
		feed.setImage(image);

		List<SyndEntry> entryList = new ArrayList<SyndEntry>();
		for (int i = 0; i != documentList.size(); i++) {
			WechatDocument document = documentList.get(i);
			SyndEntry entry = new SyndEntryImpl();
			entry.setTitle(document.getItem().getDisplay().getTitle());
			entry.setLink(document.getItem().getDisplay().getUrl());
			SyndContent content = new SyndContentImpl();
			content.setValue(document.getItem().getDisplay().getContent());
			entry.setDescription(content);
			Date date = null;
			try {
				date = format.parse(document.getItem().getDisplay().getDate());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			entry.setPublishedDate(date);
			entryList.add(entry);
		}
		feed.setEntries(entryList);

		SyndFeedOutput feedOutput = new SyndFeedOutput();
		try {
			rss = feedOutput.outputString(feed);
		} catch (FeedException e) {
			e.printStackTrace();
		}
		return rss;
	}

}
