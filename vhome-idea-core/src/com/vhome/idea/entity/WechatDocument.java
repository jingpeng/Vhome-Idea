package com.vhome.idea.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("DOCUMENT")
public class WechatDocument {
	
	@XStreamAlias("item")
	private Item item;

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
}
