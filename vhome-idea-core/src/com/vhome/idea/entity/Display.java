package com.vhome.idea.entity;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("display")
public class Display {
	@XStreamAlias("docid")
	private String docId;

	@XStreamAlias("tplid")
	private String tplId;

	@XStreamAlias("title")
	private String title;

	@XStreamAlias("url")
	private String url;

	@XStreamAlias("title1")
	private String title1;

	@XStreamAlias("imglink")
	private String imgLink;

	@XStreamAlias("headimage")
	private String headImage;

	@XStreamAlias("sourcename")
	private String sourceName;

	@XStreamAlias("content168")
	private String content168;

	@XStreamAlias("site")
	private String site;
	
	@XStreamAlias("isV")
	private boolean isV;

	@XStreamAlias("openid")
	private String openId;

	@XStreamAlias("content")
	private String content;

	@XStreamAlias("showurl")
	private String showUrl;

	@XStreamAlias("date")
	private String date;

	@XStreamAlias("lastModified")
	private long lastModified;

	@XStreamImplicit(itemFieldName = "pagesize")
	private List<String> pageSizes;

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getTplId() {
		return tplId;
	}

	public void setTplId(String tplId) {
		this.tplId = tplId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle1() {
		return title1;
	}

	public void setTitle1(String title1) {
		this.title1 = title1;
	}

	public String getImgLink() {
		return imgLink;
	}

	public void setImgLink(String imgLink) {
		this.imgLink = imgLink;
	}

	public String getHeadImage() {
		return headImage;
	}

	public void setHeadImage(String headImage) {
		this.headImage = headImage;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getContent168() {
		return content168;
	}

	public void setContent168(String content168) {
		this.content168 = content168;
	}
	
	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public boolean isV() {
		return isV;
	}

	public void setV(boolean isV) {
		this.isV = isV;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getShowUrl() {
		return showUrl;
	}

	public void setShowUrl(String showUrl) {
		this.showUrl = showUrl;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

	public List<String> getPageSizes() {
		return pageSizes;
	}

	public void setPageSizes(List<String> pageSizes) {
		this.pageSizes = pageSizes;
	}
}
