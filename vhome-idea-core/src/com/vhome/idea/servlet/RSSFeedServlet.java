package com.vhome.idea.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vhome.idea.SogouWechatOpenIdFeeder;

@SuppressWarnings("serial")
public class RSSFeedServlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SogouWechatOpenIdFeeder feeder = new SogouWechatOpenIdFeeder();
		String rss = null;
		try {
			rss = feeder.feedWechatOpenId("oIWsFt5a2J_7swhRSEqShV_QDp6U", 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.setCharacterEncoding("utf-8");
		response.getWriter().print(rss);
	}
}
