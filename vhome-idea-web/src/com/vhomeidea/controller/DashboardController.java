package com.vhomeidea.controller;

import java.util.List;

import com.jfinal.core.Controller;
import com.vhomeidea.business.SogouWechatOpenIdFeeder;
import com.vhomeidea.business.entity.WechatDocument;

public class DashboardController extends Controller {
	public void index() {
		SogouWechatOpenIdFeeder feeder = new SogouWechatOpenIdFeeder();
		List<WechatDocument> documentList = null;
		try {
			documentList = feeder.feedWechatOpenId("oIWsFt5a2J_7swhRSEqShV_QDp6U", 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		setAttr("firstLatestWeChatDocument", documentList.get(0));
		setAttr("secondLatestWeChatDocument", documentList.get(1));
		setAttr("thirdLatestWeChatDocument", documentList.get(2));
		setAttr("fourthLatestWeChatDocument", documentList.get(3));
		setAttr("fifthLatestWeChatDocument", documentList.get(4));
		setAttr("latestWeChatDocuments", documentList);
		render("blog-grids.html");
	}
}
