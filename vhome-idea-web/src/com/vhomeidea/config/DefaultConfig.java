package com.vhomeidea.config;

import com.jfinal.config.*;
import com.jfinal.render.ViewType;

import com.vhomeidea.controller.DashboardController;

public class DefaultConfig extends JFinalConfig {
	public void configConstant(Constants me) {
		me.setDevMode(true);
	}

	public void configRoute(Routes me) {
		me.add("/summarize/html/", DashboardController.class);
		me.add("/summarize/html-dark/", DashboardController.class);
		me.add("/summarize/html-light/", DashboardController.class);
	}

	public void configPlugin(Plugins me) {
	}

	public void configInterceptor(Interceptors me) {
	}

	public void configHandler(Handlers me) {
	}
}